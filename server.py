import requests
import json
robots = []
IP = "http://192.168.1.40:80/"
SIZE = 100/8


def get_cords(xx, yy):
    y = SIZE * (xx + 0.5)
    x = SIZE * (yy + 0.5)
    return [x, y]


def get_position(x, y):
    xx = int(y/SIZE)
    yy = int(x/SIZE)
    return [xx, yy]


def start():
    pass


def get_field():
    # get data from server
    response = requests.get(IP + "all/")  # Р—Р°РїСЂР°С€РёРІР°РµРј Сѓ СЃРµСЂРІРµСЂР° РґР°РЅРЅС‹Рµ
    data = response.json()

    # initialize field
    pole = []
    for i in range(8):
        pole.append([0, 0, 0, 0, 0, 0, 0, 0])

    # detect valid robots and generate field
    for i in range(len(data)):
        if 'x' in data[i][1]:
            x = get_position(data[i][1]['x'], data[i][1]['y'])[0]
            y = get_position(data[i][1]['x'], data[i][1]['y'])[1]
            team = 0
            if x in range (0, 3): team = 'black'
            if x in range (5, 8): team = 'white'
            robots.append({'ip': data[i][0], 'x': x, 'y': y, 'class': team})
            if team == 'black': pole[x][y] = 3
            if team == 'white': pole[x][y] = 1
    print(str(len(robots)) + ' robots were detected: ')
    print(robots)
    print('Field was generated: ')
    print(pole)

    return pole


def move_robot(ip, x, y):
    x = get_cords(x, y)[0]
    y = get_cords(x, y)[1]
    data = "{'ip': '"+str(ip)+"', 'x': "+str(x)+", 'y': "+str(y)+"}"
    requests.post(IP + "move/", data)