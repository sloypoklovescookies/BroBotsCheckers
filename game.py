import random
import time
import copy
import server

n2_spisok = ()  # конечный список ходов компьютера
ur = 3  # количество предсказываемых компьютером ходов
k_rez = 0  # !!!
o_rez = 0
poz1_x = -1  # клетка не задана
f_hi = True  # определение хода игрока(да)


def start():  # начало игры, точка входа
    global pole
    pole = server.get_field()
    global robots
    robots = server.robots


def soobsenie(s):
    global f_hi
    if s == 1:
        print('Вы победили')
    if s == 2:
        print('Вы проиграли')
    if s == 3:
        print('Ничья')
    exit()


def pozici_1():  # выбор клетки для хода 1
    print('Ваш ход')
    x = int(input("X: "))
    y = int(input("Y: "))


def pozici_2():  # выбор клетки для хода 2
    global poz1_x, poz1_y, poz2_x, poz2_y
    global f_hi
    x = int(input("X: "))
    y = int(input("Y: "))
    if pole[y][x] == 1 or pole[y][x] == 2:  # проверяем пешку игрока в выбранной клетке
        poz1_x, poz1_y = x, y
    else:
        if poz1_x != -1:  # клетка выбрана
            poz2_x, poz2_y = x, y
            if f_hi:  # ход игрока?
                hod_igroka()
                if not (f_hi):
                    time.sleep(0.5)
                    hod_kompjutera()  # передаём ход компьютеру
            poz1_x = -1  # клетка не выбрана


def hod_kompjutera():  # !!!
    global f_hi
    global n2_spisok
    proverka_hk(1, (), [])
    if n2_spisok:  # проверяем наличие доступных ходов
        kh = len(n2_spisok)  # количество ходов
        th = random.randint(0, kh - 1)  # случайный ход
        dh = len(n2_spisok[th])  # длина хода
        for h in n2_spisok:  # !!!для отладки!!!
            h = h  # !!!для отладки!!!
        for i in range(dh - 1):
            # выполняем ход
            spisok = hod(1, n2_spisok[th][i][0], n2_spisok[th][i][1], n2_spisok[th][1 + i][0], n2_spisok[th][1 + i][1])
            time.sleep(5)  # ждем, пока робот переместится
        n2_spisok = []  # очищаем список ходов
        f_hi = True  # ход игрока доступен

    # определяем победителя
    s_k, s_i = skan()
    if not (s_i):
        soobsenie(2)
    elif not (s_k):
        soobsenie(1)
    elif f_hi and not (spisok_hi()):
        soobsenie(3)
    elif not (f_hi) and not (spisok_hk()):
        soobsenie(3)


def spisok_hk():  # составляем список ходов компьютера
    spisok = prosmotr_hodov_k1([])  # здесь проверяем обязательные ходы
    if not (spisok):
        spisok = prosmotr_hodov_k2([])  # здесь проверяем оставшиеся ходы
    return spisok


def proverka_hk(tur, n_spisok, spisok):  # !!!
    global pole
    global n2_spisok
    global l_rez, k_rez, o_rez
    if not (spisok):  # если список ходов пустой...
        spisok = spisok_hk()  # заполняем

    if spisok:
        k_pole = copy.deepcopy(pole)  # копируем поле
        for ((poz1_x, poz1_y), (poz2_x, poz2_y)) in spisok:  # проходим все ходы по списку
            t_spisok = hod(0, poz1_x, poz1_y, poz2_x, poz2_y)
            if t_spisok:  # если существует ещё ход
                proverka_hk(tur, (n_spisok + ((poz1_x, poz1_y),)), t_spisok)
            else:
                proverka_hi(tur, [])
                if tur == 1:
                    t_rez = o_rez / k_rez
                    if not (n2_spisok):  # записыаем если пустой
                        n2_spisok = (n_spisok + ((poz1_x, poz1_y), (poz2_x, poz2_y)),)
                        l_rez = t_rez  # сохряняем наилучший результат
                    else:
                        if t_rez == l_rez:
                            n2_spisok = n2_spisok + (n_spisok + ((poz1_x, poz1_y), (poz2_x, poz2_y)),)
                        if t_rez > l_rez:
                            n2_spisok = ()
                            n2_spisok = (n_spisok + ((poz1_x, poz1_y), (poz2_x, poz2_y)),)
                            l_rez = t_rez  # сохряняем наилучший результат
                    o_rez = 0
                    k_rez = 0

            pole = copy.deepcopy(k_pole)  # возвращаем поле
    else:  # ???
        s_k, s_i = skan()  # подсчёт результата хода
        o_rez += (s_k - s_i)
        k_rez += 1


def spisok_hi():  # составляем список ходов игрока
    spisok = prosmotr_hodov_i1([])  # здесь проверяем обязательные ходы
    if not (spisok):
        spisok = prosmotr_hodov_i2([])  # здесь проверяем оставшиеся ходы
    return spisok


def proverka_hi(tur, spisok):
    global pole, k_rez, o_rez
    global ur
    if not (spisok):
        spisok = spisok_hi()

    if spisok:  # проверяем наличие доступных ходов
        k_pole = copy.deepcopy(pole)  # копируем поле
        for ((poz1_x, poz1_y), (poz2_x, poz2_y)) in spisok:
            t_spisok = hod(0, poz1_x, poz1_y, poz2_x, poz2_y)
            if t_spisok:  # если существует ещё ход
                proverka_hi(tur, t_spisok)
            else:
                if tur < ur:
                    proverka_hk(tur + 1, (), [])
                else:
                    s_k, s_i = skan()  # подсчёт результата хода
                    o_rez += (s_k - s_i)
                    k_rez += 1

            pole = copy.deepcopy(k_pole)  # возвращаем поле
    else:  # доступных ходов нет
        s_k, s_i = skan()  # подсчёт результата хода
        o_rez += (s_k - s_i)
        k_rez += 1


def skan():  # подсчёт пешек на поле
    global pole
    s_i = 0
    s_k = 0
    for i in range(8):
        for ii in pole[i]:
            if ii == 1: s_i += 1
            if ii == 2: s_i += 3
            if ii == 3: s_k += 1
            if ii == 4: s_k += 3
    return s_k, s_i


def hod_igroka():
    global poz1_x, poz1_y, poz2_x, poz2_y
    global f_hi
    f_hi = False  # считаем ход игрока выполненным
    spisok = spisok_hi()
    if spisok:
        if ((poz1_x, poz1_y), (poz2_x, poz2_y)) in spisok:  # проверяем ход на соответствие правилам игры
            t_spisok = hod(1, poz1_x, poz1_y, poz2_x, poz2_y)  # если всё хорошо, делаем ход
            time.sleep(5) # ждем, пока робот переместится
            if t_spisok:  # если есть ещё ход той же пешкой
                f_hi = True  # считаем ход игрока невыполненным
        else:
            f_hi = True  # считаем ход игрока невыполненным


def hod(f, poz1_x, poz1_y, poz2_x, poz2_y):
    global pole
    # "рубим" робота, на позицию которого совершается ход
    for i in range(len(robots)):
        if robots[i]['x'] == poz2_x and robots[i]['y'] == poz2_y:
            server.move_robot(robots[i]['ip'], poz2_x, 110)  # убираем робота с поля
            # убираем робота из списка
            robots.remove(robots[i])
            break
    # передвигаем ходящего робота
    for i in range(len(robots)):
        if robots[i]['x'] == poz1_x and robots[i]['y'] == poz1_y:
            server.move_robot(robots[i]['ip'], poz2_x, poz2_y)  # перемещаем робота
            # присваиваем роботу новые координаты
            robots[i]['x'] = poz2_x
            robots[i]['y'] = poz2_y
            break

    # превращение
    if poz2_y == 0 and pole[poz1_y][poz1_x] == 1:
        pole[poz1_y][poz1_x] = 2
    # превращение
    if poz2_y == 7 and pole[poz1_y][poz1_x] == 3:
        pole[poz1_y][poz1_x] = 4
    # делаем ход
    pole[poz2_y][poz2_x] = pole[poz1_y][poz1_x]
    pole[poz1_y][poz1_x] = 0

    # рубим пешку игрока
    kx = ky = 1
    if poz1_x < poz2_x: kx = -1
    if poz1_y < poz2_y: ky = -1
    x_poz, y_poz = poz2_x, poz2_y
    while (poz1_x != x_poz) or (poz1_y != y_poz):
        x_poz += kx
        y_poz += ky
        if pole[y_poz][x_poz] != 0:
            pole[y_poz][x_poz] = 0
            # проверяем ход той же пешкой...
            if pole[poz2_y][poz2_x] == 3 or pole[poz2_y][poz2_x] == 4:  # ...компьютера
                return prosmotr_hodov_k1p([], poz2_x, poz2_y)  # возвращаем список доступных ходов
            elif pole[poz2_y][poz2_x] == 1 or pole[poz2_y][poz2_x] == 2:  # ...игрока
                return prosmotr_hodov_i1p([], poz2_x, poz2_y)  # возвращаем список доступных ходов


def prosmotr_hodov_k1(spisok):  # проверка наличия обязательных ходов
    for y in range(8):  # сканируем всё поле
        for x in range(8):
            spisok = prosmotr_hodov_k1p(spisok, x, y)
    return spisok


def prosmotr_hodov_k1p(spisok, x, y):
    if pole[y][x] == 3:  # пешка
        for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
            if 0 <= y + iy + iy <= 7 and 0 <= x + ix + ix <= 7:
                if pole[y + iy][x + ix] == 1 or pole[y + iy][x + ix] == 2:
                    if pole[y + iy + iy][x + ix + ix] == 0:
                        spisok.append(((x, y), (x + ix + ix, y + iy + iy)))  # запись хода в конец списка
    if pole[y][x] == 4:  # пешка с короной
        for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
            osh = 0  # определение правильности хода
            for i in range(1, 8):
                if 0 <= y + iy * i <= 7 and 0 <= x + ix * i <= 7:
                    if osh == 1:
                        spisok.append(((x, y), (x + ix * i, y + iy * i)))  # запись хода в конец списка
                    if pole[y + iy * i][x + ix * i] == 1 or pole[y + iy * i][x + ix * i] == 2:
                        osh += 1
                    if pole[y + iy * i][x + ix * i] == 3 or pole[y + iy * i][x + ix * i] == 4 or osh == 2:
                        if osh > 0: spisok.pop()  # удаление хода из списка
                        break
    return spisok


def prosmotr_hodov_k2(spisok):  # проверка наличия остальных ходов
    for y in range(8):  # сканируем всё поле
        for x in range(8):
            if pole[y][x] == 3:  # пешка
                for ix, iy in (-1, 1), (1, 1):
                    if 0 <= y + iy <= 7 and 0 <= x + ix <= 7:
                        if pole[y + iy][x + ix] == 0:
                            spisok.append(((x, y), (x + ix, y + iy)))  # запись хода в конец списка
                        if pole[y + iy][x + ix] == 1 or pole[y + iy][x + ix] == 2:
                            if 0 <= y + iy * 2 <= 7 and 0 <= x + ix * 2 <= 7:
                                if pole[y + iy * 2][x + ix * 2] == 0:
                                    spisok.append(((x, y), (
                                    x + ix * 2, y + iy * 2)))  # запись хода в конец списка
            if pole[y][x] == 4:  # пешка с короной
                for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
                    osh = 0  # определение правильности хода
                    for i in range(1, 8):
                        if 0 <= y + iy * i <= 7 and 0 <= x + ix * i <= 7:
                            if pole[y + iy * i][x + ix * i] == 0:
                                spisok.append(((x, y), (x + ix * i, y + iy * i)))  # запись хода в конец списка
                            if pole[y + iy * i][x + ix * i] == 1 or pole[y + iy * i][x + ix * i] == 2:
                                osh += 1
                            if pole[y + iy * i][x + ix * i] == 3 or pole[y + iy * i][x + ix * i] == 4 or osh == 2:
                                break
    return spisok


def prosmotr_hodov_i1(spisok):  # проверка наличия обязательных ходов
    spisok = []  # список ходов
    for y in range(8):  # сканируем всё поле
        for x in range(8):
            spisok = prosmotr_hodov_i1p(spisok, x, y)
    return spisok


def prosmotr_hodov_i1p(spisok, x, y):
    if pole[y][x] == 1:  # пешка
        for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
            if 0 <= y + iy + iy <= 7 and 0 <= x + ix + ix <= 7:
                if pole[y + iy][x + ix] == 3 or pole[y + iy][x + ix] == 4:
                    if pole[y + iy + iy][x + ix + ix] == 0:
                        spisok.append(((x, y), (x + ix + ix, y + iy + iy)))  # запись хода в конец списка
    if pole[y][x] == 2:  # пешка с короной
        for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
            osh = 0  # определение правильности хода
            for i in range(1, 8):
                if 0 <= y + iy * i <= 7 and 0 <= x + ix * i <= 7:
                    if osh == 1:
                        spisok.append(((x, y), (x + ix * i, y + iy * i)))  # запись хода в конец списка
                    if pole[y + iy * i][x + ix * i] == 3 or pole[y + iy * i][x + ix * i] == 4:
                        osh += 1
                    if pole[y + iy * i][x + ix * i] == 1 or pole[y + iy * i][x + ix * i] == 2 or osh == 2:
                        if osh > 0: spisok.pop()  # удаление хода из списка
                        break
    return spisok


def prosmotr_hodov_i2(spisok):  # проверка наличия остальных ходов
    for y in range(8):  # сканируем всё поле
        for x in range(8):
            if pole[y][x] == 1:  # пешка
                for ix, iy in (-1, -1), (1, -1):
                    if 0 <= y + iy <= 7 and 0 <= x + ix <= 7:
                        if pole[y + iy][x + ix] == 0:
                            spisok.append(((x, y), (x + ix, y + iy)))  # запись хода в конец списка
                        if pole[y + iy][x + ix] == 3 or pole[y + iy][x + ix] == 4:
                            if 0 <= y + iy * 2 <= 7 and 0 <= x + ix * 2 <= 7:
                                if pole[y + iy * 2][x + ix * 2] == 0:
                                    spisok.append(((x, y), (
                                    x + ix * 2, y + iy * 2)))  # запись хода в конец списка
            if pole[y][x] == 2:  # пешка с короной
                for ix, iy in (-1, -1), (-1, 1), (1, -1), (1, 1):
                    osh = 0  # определение правильности хода
                    for i in range(1, 8):
                        if 0 <= y + iy * i <= 7 and 0 <= x + ix * i <= 7:
                            if pole[y + iy * i][x + ix * i] == 0:
                                spisok.append(((x, y), (x + ix * i, y + iy * i)))  # запись хода в конец списка
                            if pole[y + iy * i][x + ix * i] == 3 or pole[y + iy * i][x + ix * i] == 4:
                                osh += 1
                            if pole[y + iy * i][x + ix * i] == 1 or pole[y + iy * i][x + ix * i] == 2 or osh == 2:
                                break
    return spisok


start()


